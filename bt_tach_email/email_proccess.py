def emailProcess(email):
    email_username = email[:email.index("@")]
    email_domain = email[email.index("@")+1:]
    return [email_username, email_domain]

def printMsg(user, domain):
    print(f"Username is: {user}; Domain is: {domain}")

def main():
    email = input("Please enter your email address: ").strip()
    email_username, email_domain = emailProcess(email) 
    printMsg(email_username, email_domain)

if __name__ == "__main__":
    main()
