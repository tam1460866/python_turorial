from email_proccess import emailProcess, printMsg

def main():
    emails = ["tam123@gmail.com", "tam54321@gmail.com", "hoangAn@gmail.com"]
    for email in emails:
        username, domain = emailProcess(email)
        printMsg(username, domain)

if __name__ == "__main__":
    main()
