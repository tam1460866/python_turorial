'''
Mutable: kieu du lieu thay doi duoc
   list
   dict
   set
   bytearray
'''

'''
Immutable: kieu du lieu khong thay doi dc
    int, float, decimal
    bool, bytes
    tuple, range, frozenset
    string, complex
'''