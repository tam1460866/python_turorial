from sub_module import func1, welcome

def wellcome():
    name = __name__
    print(f"WELCOME TO {name}")

def main():
    wellcome()
    welcome()
    func1()

main()

# File duoc chay chinh se co __name__ = '__main__'