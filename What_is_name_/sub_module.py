def welcome():
    name = __name__
    print(f"WELCOME TO {name}")

def func1():
    print("this is func 1")

def main():
    welcome()
    func1()
    
if __name__ == "__main__":
    main()