'''
    Tuple trong Python

    Kiểu dữ liệu Tuple trong python là một collection có thứ tự, không thể thay đổi. 
    Cho phép chứa dữ liệu trùng lặp.
    Tuple sử dụng các dấu ngoặc đơn, Không giống như List sử dụng các dấu ngoặc vuông. 
    Các đối tượng trong tuple được phân biệt bởi dấu phảy và được bao quanh bởi dấu ngoặc đơn (). 
    Giống như chỉ mục của chuỗi, chỉ mục của tuple bắt đầu từ 0.
'''

'''
    Creating a Tuple
'''
#====================================================================================================
# Creating an empty Tuple
Tuple1 = ()
print("Initial empty Tuple: ")
print(Tuple1) # ()

# Creating a Tuple
# with the use of string
Tuple1 = ('Geeks', 'For')
print("\nTuple with the use of String: ")
print(Tuple1)    # ('Geeks', 'For')

# Creating a Tuple with
# the use of list
list1 = [1, 2, 4, 5, 6]
print("\nTuple using List: ")
print(tuple(list1))  # (1, 2, 4, 5, 6)

# Creating a Tuple
# with the use of built-in function
Tuple1 = tuple('Geeks')
print("\nTuple with the use of function: ")
print(Tuple1)    # ('G', 'e', 'e', 'k', 's') 

# Creating a Tuple
# with Mixed Datatype
Tuple1 = (5, 'Welcome', 7, 'Geeks')
print("\nTuple with Mixed Datatypes: ")
print(Tuple1)    # (5, 'Welcome', 7, 'Geeks')

# Creating a Tuple
# with nested tuples
Tuple1 = (0, 1, 2, 3)
Tuple2 = ('python', 'geek')
Tuple3 = (Tuple1, Tuple2)
print("\nTuple with nested tuples: ")
print(Tuple3)  # ((0, 1, 2, 3), ('python', 'geek'))
Tuple4 = (*Tuple1, *Tuple2)
print(Tuple4)  # (0, 1, 2, 3, 'python', 'geek')

'''
    Accessing of Tuples
'''

# with Indexing
Tuple1 = tuple("Geeks")
print("\nFirst element of Tuple: ")
print(Tuple1[0])  # G
print(Tuple1[-1])  # s
print(Tuple1[::-1])  # ('s', 'k', 'e', 'e', 'G')
 
 #           0         1        2         3        4       5
fruits = ("apple", "banana", "guava", "kiwi", "cherry", "orange", "lemon")
print (fruits[2:5])   # ('guava', 'kiwi', 'cherry')

 #         -7         -6       -5       -4       -3       -2        -1
fruits = ("apple", "banana", "guava", "kiwi", "cherry", "orange", "lemon")
print (fruits[-5:-2])   # ('guava', 'kiwi', 'cherry')

# Tuple unpacking
Tuple1 = ("Geeks", "For", "Geeks")
a, b, c = Tuple1
print("\nValues after unpacking: ")
print(a)   # Geeks
print(b)   # For
print(c)   # Geeks

'''
    Concatenation of Tuples

    Nối bộ tuple là quá trình nối hai hoặc nhiều Tuple. 
    Kết nối được thực hiện bằng cách sử dụng toán tử +
    Các phép toán số học khác không áp dụng trên Tuples.
'''
Tuple1 = (0, 1, 2, 3)
Tuple2 = ('Geeks', 'For', 'Geeks')
Tuple3 = Tuple1 + Tuple2
print("\nTuples after Concatenation: ")
print(Tuple3)   # (0, 1, 2, 3, 'Geeks', 'For', 'Geeks')


'''
    Slicing of Tuple
'''
# 0   1  2  3  4  5  6  7  8  9  10 11 12                               
# G   E  E  K  S  F  O  R  G  E  E  K  S
#-13 -12-11-10 -9 -8 -7 -6 -5 -4 -3 -2 -1   
               
Tuple1 = tuple('GEEKSFORGEEKS')
print(Tuple1[1:])     # ('E', 'E', 'K', 'S', 'F', 'O', 'R', 'G', 'E', 'E', 'K', 'S')
print(Tuple1[::-1])   # ('S', 'K', 'E', 'E', 'G', 'R', 'O', 'F', 'S', 'K', 'E', 'E', 'G')
print(Tuple1[4:9])    # ('S', 'F', 'O', 'R', 'G')

'''
    Deleting a Tuple

    Tuples là bất biến và do đó chúng không cho phép xóa một phần của nó. 
    Toàn bộ tuple bị xóa bằng cách sử dụng phương thức del ().
'''
# Deleting a Tuple
 
Tuple1 = (0, 1, 2, 3, 4)
del Tuple1
 
# print(Tuple1)  NameError: name ‘Tuple1’ is not defined

'''
    Editing a Tuple

    Khi một tuple được tạo, bạn không thể thay đổi giá trị của nó. 
    Tuple là không thể thay đổi hoặc bất biến.
    Có một cách giải quyết. Đó là bạn có thể chuyển đổi tuple thành một list, 
    thay đổi list và chuyển đổi lại thành tuple.
'''
x = ("apple", "banana", "cherry")
y = list(x)
y[1] = "kiwi"
x = tuple(y)
 
print(x) # ('apple', 'kiwi', 'cherry')

'''
    Iterable List
'''

fruits = ("apple", "banana", "cherry")
for x in fruits:
    print(x)

fruits = ("apple", "banana", "cherry")
for i, x in enumerate(fruits):
    print(i, x)


''' ------------------------------------------
    Built-In Methods
    ------------------------------------------
''' 

'''
    Index()

    seq.index(element, start, end)

        Parameters:  
            + element – The element whose lowest index will be returned.
            + start (Optional) – The position from where the search begins.
            + end (Optional) – The position from where the search ends.
        Returns:  
            + Returns the lowest index where the element appears.
        Error: 
            + If any element which is not present is searched, it returns a ValueError
'''
tup = (1, 2, 3, 4, 1, 1, 1, 4, 5)
print(tup.index(4))   # 3
print(tup.index(4, 5))# 7
#print(tup.index(4, 4, 6)) # ValueError: tuple.index(x): x not in tuple

tup2 = ['cat', 'bat', 'mat', 'cat', 'pet']
print(tup2.index('cat'))  # 0

tup3 = (1, 2, 3, [9, 8, 7], ('cat', 'bat')) 
# Will print the index of sublist [9, 8, 7]
print(tup3.index([9, 8, 7]))  # 3

'''
    Count()

    Syntax: seq.count(object) 

    Parameters: 
        Đối tượng là những thứ có số lượng được trả lại.
    Returns: 
        count() phương thức trả về số lần đối tượng xuất hiện trong danh sách.
    Exception: 
        Nếu nhiều hơn thì 1 tham số được truyền vào phương thức count (), nó sẽ trả về một TypeError.
'''
list1 = (1, 1, 1, 2, 3, 2, 1)
print(list1.count(1)) # 4
 
list2 = ['a', 'a', 'a', 'b', 'b', 'a', 'c', 'b']
print(list2.count('b')) # 3
 
list3 = ('Cat', 'Bat', 'Sat', 'Cat', 'cat', 'Mat')
print(list3.count('Cat')) # 2

list1 = (1, 1, 1, 2, 3, 2, 1)
# Error when two parameters is passed.
#print(list1.count(1, 2))

list1 = (('Cat', 'Bat'), ('Sat', 'Cat'), ('Cat', 'Bat'),
          ('Cat', 'Bat', 'Sat'), [1, 2], [1, 2, 3], [1, 2])
print(list1.count(('Cat', 'Bat')))  # 2
print(list1.count([1, 2]))          # 2

lst = ['Cat', 'Bat', 'Sat', 'Cat', 'Mat', 'Cat', 'Sat']
print ([(l, lst.count(l)) for l in set(lst)]) # [('Cat', 3), ('Bat', 1), ('Mat', 1), ('Sat', 2)]
print (dict( (l, lst.count(l) ) for l in set(lst)))  # {'Bat': 1, 'Cat': 3, 'Sat': 2, 'Mat': 1}