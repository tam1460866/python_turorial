'''
Strings (Choi ky tu) ----------------------------------------->    
'''

my_string1 = 'tam so ciu'
my_string2 = "tam so ciu 2"
my_string3 = "I'm tam so ciu"
my_string4 = 'I\'m "Tam so hot"'
my_string5 = "I'm \"Tam so hot\""
print(my_string1, type(my_string1))
print(my_string2)
print(my_string3)
print(my_string4)
print(my_string5)

# backflash if you want continous in the next line (xuong dong trong code nhung chuoi van la 1 dong)
my_string = "Cloud đang là xu hướng công nghệ năm 2021 và AWS trở thành nhà cung \
cấp nền tảng Cloud được sử dụng rộng rãi nhất."
print(my_string)


'''
    Access Characters ----------------------------------------->    
'''
#            012345678910
my_string = "Hello World"
#                    -2-1

print(my_string[0])    # H
print(my_string[4])    # o
print(my_string[-1])   # d
print(my_string[-2])   # l


'''
    Substrings ----------------------------------------->    
    str[startIndex : endIndex]
    str[1:5] -> start at index 1 go until index 4 (not include 5)
    str[:5] -> start at index 0 go until index 4 (not include 5)
    str[1:] -> start at index 1 go end of str
'''
#            012345678910
my_string = "Hello World"
#                    -2-1
print(my_string[1:5])   # ello
print(my_string[:5])    # Hello
print(my_string[1:])    # ello  World
print(my_string[::-1])  # dlroW olleH --> lấy mỗi kí tự từ vị trí -1
print(my_string[::1])   # Hello World --> lấy mỗi kí tự từ vị trí 1
print(my_string[::2])   # HloWrd --> mỗi 2 kí tự thi lấy 1

'''
    Reverse Str ----------------------------------------->    
'''
#            012345678910
my_string = "Hello World"
#                    -2-1
print(my_string[::-1])  # dlroW olleH


'''
    Concatenate Str ----------------------------------------->    
'''
# concat string with +
str1 = "hello xin chao"
str2 = "tam de thuong"
str3 = "da den voi Rakumo"

sentence1 = str1 + str2 + " ban rat dep"
print(sentence1)

# concat string with .join
sentence2 = ' '.join([str1, str2, str3])
print(sentence2)
sentence3 = '*'.join([str1, str2, str3])
print(sentence3)


'''
    Iterating over a character in string -----------------------------------------> 
'''
my_string = "Hello World"

for c in my_string:
    print(c)



'''
    Check a character in string -----------------------------------------> 
'''
my_string = "Hello World"
condition1 = 'H' in my_string  # True
print(condition1)
condition1 = 'h' in my_string  # False
print(condition1)
condition1 = 'p' in my_string  # False
print(condition1)
condition1 = 'ello' in my_string  # True
print(condition1)

'''
    Strips whitespace character in string -----------------------------------------> 
    strip(), lstrip(), rstrip()
'''
# strip()
s1 = '   I am alone     '.strip()
s2 = 'OOOI am aloneOOOOOO'.strip('O')
s3 = '------I am alone========'.strip('-=')
print(s1)  # 'I am alone'
print(s2)  # 'I am alone'
print(s3)  # 'I am alone'

# lstrip()
s = 'OOOI am aloneOOOOOO'.lstrip('O')
print(s)   # I am aloneOOOOOO

# rstrip()
s = 'OOOI am aloneOOOOOO'.rstrip('O')
print(s)   # OOOI am alone


'''
    String Formatting ----------------------------------------->  
    %, .format(), f-String
'''
# use %
name = "CodeXplore"
myString = "Welcom to %s" % name
print(myString)

pi = 3.14159
s = "pi number"
my_string = "Chu vi hinh tron su dung so %s is %f" % (s,pi)
print(my_string)
my_string = "Chu vi hinh tron su dung so %s is %.2f" % (s,pi)
print(my_string)

# use .format()
age = 30
height = 170.123
name = "tamsociu"

my_string = "my name is {} , I \'m {} years old and i tall {} cm".format(name, age, height)
print(my_string)
my_string = "my name is {} , I \'m {} years old and i tall {:.2f} cm".format(name, age, height)
print(my_string)

# use f-String (python > 3.6)
pi = 3.14159
name = "tamsociu"
my_string = f"pi is {pi:.2f} and my name is {name}"
print(my_string)

'''
    String is immutable -> cannot be change ----------------------------------------->    
'''
# my_string = "Hello World"
# my_string[0] = 'M' --> error vi string ko the thay doi
