'''
    - Kiểu dữ liệu Dictionary trong Python là một tập hợp các cặp key-value không có thứ tự, 
        có thể thay đổi và lập chỉ mục (truy cập phần tử theo chỉ mục). 
    - Dictionary được khởi tạo với các dấu ngoặc nhọn {} và chúng có các khóa và giá trị (key-value). 
    - Mỗi cặp key-value được xem như là một item. Key mà đã truyền cho item đó phải là duy nhất, 
        trong khi đó value có thể là bất kỳ kiểu giá trị nào. Key phải là một kiểu dữ liệu không 
        thay đổi (immutable) như chuỗi, số hoặc tuple.
'''


'''
    Creating a Dictionary
'''

Dict = {1: 'Geeks', 2: 'For', 3: 'Geeks'}
print(Dict)   # {1: 'Geeks', 2: 'For', 3: 'Geeks'}

Dict = {'Name': 'Geeks', 1: [1, 2, 3, 4]}
print(Dict)   # {'Name': 'Geeks', 1: [1, 2, 3, 4]}

Dict = {}
print(Dict)   # {}

Dict = {1: 'Geeks', 2: 'For', 3:{'A' : 'Welcome', 'B' : 'To', 'C' : 'Geeks'}}
print(Dict)   # {1: 'Geeks', 2: 'For', 3: {'A': 'Welcome', 'B': 'To', 'C': 'Geeks'}}

Dict = dict({1: 'Geeks', 2: 'For', 3:'Geeks'})
print(Dict)   # {1: 'Geeks', 2: 'For', 3: 'Geeks'}

Dict = dict([(1, 'Geeks'), (2, 'For')])
print(Dict)   # {1: 'Geeks', 2: 'For'}

Dict = dict(name='tam', age=24, height=175)
print(Dict)   # {'name': 'tam', 'age': 24, 'height': 175}


'''
    Iterate Dict
'''
Dict = {1: 'Geeks', 'name': 'For', '3': 'GeeksGeeks'}
for key, val in Dict.items():
    print(key, val)

for val in Dict.values():
    print(val)

for key in Dict.keys():
    print(key, Dict[key])



''' 
    Accessing elements 
        dict[key]
        dict.get(key, default_value)
'''

Dict = {1: 'Geeks', 'name': 'For', '3': 'GeeksGeeks'}
print(Dict['name'])  # For
print(Dict[1])       # Geeks
print(Dict['3'])     # GeeksGeeks
# print(Dict['100']) # KeyError

Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
print(Dict.get(3))  # Geeks
print(Dict.get('name', 'dafault'))  # For
print(Dict.get('tam', 'default'))   # default

Dict = {'Dict1': {1: 'Geeks'},'Dict2': {'Name': 'For'}}
 
print(Dict['Dict1'])         # {1: 'Geeks'}
print(Dict['Dict1'][1])      # Geeks
print(Dict['Dict2']['Name']) # For

'''
    Adding & update elements
        dict[key] = val : add new key-val neu key khong ton tai trong dict, 
                          neu key da ton tai se update value cua key do
        dict.update()
'''
# Creating an empty Dictionary
Dict = {}
print(Dict)   # {}
 
# Adding elements one at a time
Dict[0] = 'Geeks'
Dict[2] = 'For'
Dict[3] = 1
print(Dict)   # {0: 'Geeks', 2: 'For', 3: 1}
 
Dict['Value_set'] = 2, 3, 4
print(Dict)   # {0: 'Geeks', 2: 'For', 3: 1, 'Value_set': (2, 3, 4)}
 
# Updating existing Key's Value
Dict[2] = 'Welcome'
print(Dict)   # {0: 'Geeks', 2: 'Welcome', 3: 1, 'Value_set': (2, 3, 4)}
 
# Adding Nested Key value to Dictionary
Dict.update({'Nested' :{'1' : 'Life', '2' : 'Geeks'}})
print(Dict)   # {0: 'Geeks', 2: 'Welcome', 3: 1, 'Value_set': (2, 3, 4), 
              #   'Nested': {'1': 'Life', '2': 'Geeks'}}


'''
    Removing Elements
        - del dict[key]: xoa 1 cap key_value khoi dict
        - pop(key): được sử dụng để trả về và xóa giá trị của khóa được chỉ định.
        - popitem(): trả về và xóa một cặp phần tử (khóa, giá trị) tùy ý khỏi từ điển.
                     tu python >= 3.7 popitem xoa phan tu cuoi cung dc them vao dict
        - clear(): xoa het cac element in dict
'''

Dict = { 5 : 'Welcome', 6 : 'To', 7 : 'Geeks',
        'A' : {1 : 'Geeks', 2 : 'For', 3 : 'Geeks'},
        'B' : {1 : 'Geeks', 2 : 'Life'}}
 
# Deleting a Key value
del Dict[6]
print(Dict)   # {'A': {1: 'Geeks', 2: 'For', 3: 'Geeks'}, 'B': {1: 'Geeks', 2: 'Life'}, 5: 'Welcome', 7: 'Geeks'}

del Dict['A'][2]
print(Dict)   # {'A': {1: 'Geeks', 3: 'Geeks'}, 'B': {1: 'Geeks', 2: 'Life'}, 5: 'Welcome', 7: 'Geeks'}


Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
# pop_ele = Dict.pop() TypeError: pop expected at least 1 argument, got 0

Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
pop_ele = Dict.pop('name')
print(pop_ele)   # For
print(Dict)      # {1: 'Geeks', 3: 'Geeks'}

Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
pop_ele = Dict.popitem()
print(pop_ele)   # (3, 'Geeks')
print(Dict)      # {1: 'Geeks', 'name': 'For'}

Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
Dict.clear()
print(Dict)  # {}
