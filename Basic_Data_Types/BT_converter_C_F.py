def cToFConverter():
    while True:
        cTemp = input("Please enter C degree: ")
        if cTemp:
            if cTemp.isdecimal():
                cTemp = float(cTemp)
                fTemp = round(9*cTemp/5+32, 1)
                print(f"{cTemp}C is converter to {fTemp}F")
                break
            else:
                print("cTemp input is not decimal")
                continue
        else:
            print("cTemp input is empty")
            continue

def main():
    cToFConverter()

if __name__ == '__main__':
    main()