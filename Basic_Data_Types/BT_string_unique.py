'''
    Check chuoi str co unique khong (cac ky tu khong lap lai)
'''
import unittest

def unique(str):
    char_set = {}
    for char in str:
        if char in char_set:
            return False
        char_set[char] = True
    return True

class Test(unittest.TestCase):
    dataT = [("asdfgh"), ("hgfds"), ("098ujkm")]
    dataF = [("sdfdfg"), ("jhkii"), ("dgdgd989")]

    def test_unique(self):
        # True check
        for t in self.dataT:
            actualResult = unique(t)
            self.assertTrue(actualResult)
        # False check
        for t in self.dataF:
            actualResult = unique(t)
            self.assertFalse(actualResult)

if __name__ ==  '__main__':
    unittest.main()