'''
    Basics Data Types:
    bool, None, int, float, str
'''

'''
    bool: True or False
'''
var_bool_true = True
var_bool_false = False
print(type(var_bool_true))

a = 4 + True
print(a) 
# a = 4 + 1 = 5

b = 4 + False
print(b) 
# b = 4 + 0 = 4

'''
    None: NoneType
    None tuong duong voi False khi so sanh codition
'''
email = None
print(type(email))

if email:
    print(f"Your email is: {email}")
else:
    print(f"Your email is empty or {email}")

'''
    Number: int & float
    Numbers: int (Integer = so nguyen) & float (Floating point number = So Thuc
'''

print(type(0), type(1), type(-10))  #int
print(type(0.0), type(2.3), type(4E2)) #float 4E2 = 4*10(^2)


'''
    Arithmetic: cac phep toan
    + : cong
    - : tru
    * : nhan
    / : chia lay thap phan
    **: mu?
    //: chia lay nguyen
    % : chia lay du
'''
print(10+3)   # 13
print(10-3)   # 7
print(10*3)   # 30
print(10**3)  # 1000
print(10/3)   # 3.33333
print(10//3)  # 3
print(10%3)   # 1

print("BASCIC FUNCTION: ")
print(pow(10,3))   # 10^3 = 1000
print(abs(-6.9))   # 6.9 gia tri tuyet doi
print(round(6.69)) # 7 lam tron len
print(round(6.668, 2))# 6,67 lam tron len den gia tri thap phan thu 2
print(bin(512))       # '0b1000000000' ---> binary format
print(hex(521))       # '0x200' ---> hexadecimal format 