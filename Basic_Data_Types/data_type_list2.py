'''
    List Comprehension
        + list[<action> for <item> in <iterator> if <some condition>]
'''
even_numbers = [i for i in range(0, 10) if i % 2 == 0]
print(even_numbers)

matrix = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]
lst = [matrix[row][col] for row in range(len(matrix)) for col in range(len(matrix))]
print(lst)

'''
    Zip List
        Syntax :  zip(*iterators) 
        Parameters : Python iterables or containers ( list, string etc ) 
        Return Value : Returns a single iterator object, having mapped values from all the 
                       containers.
'''
 
name = [ "Manjeet", "Nikhil", "Shambhavi", "Astha" ]
roll_no = [ 4, 1, 3, 2 ]
mapped = zip(name, roll_no)
print(set(mapped))  # {('Nikhil', 1), ('Shambhavi', 3), ('Manjeet', 4), ('Astha', 2)}

name = [ "Manjeet", "Nikhil", "Shambhavi"]
roll_no = [ 4, 1, 3, 2 , 9, 1, 0]
mapped = zip(name, roll_no)
print(set(mapped))  # {('Nikhil', 1), ('Shambhavi', 3), ('Manjeet', 4)}

names = ['Mukesh', 'Roni', 'Chari']
ages = [24, 50, 18]
for i, (name, age) in enumerate(zip(names, ages)):
    print(i, name, age)

stocks = ['reliance', 'infosys', 'tcs']
prices = [2175, 1127, 2750]
new_dict = {stocks: prices for stocks, prices in zip(stocks, prices)}
print(new_dict)  # {'reliance': 2175, 'infosys': 1127, 'tcs': 2750}

players = ["Sachin", "Sehwag", "Gambhir", "Dravid", "Raina"]
scores = [100, 15, 17, 28, 43]
for pl, sc in zip(players, scores):
    print("Player :  %s     Score : %d" % (pl, sc))

matrix = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]
mapped = list(zip(*matrix)) 
print(mapped)      # [(1, 4, 7), (2, 5, 8), (3, 6, 9)]

# Un zip()
name = ["Manjeet", "Nikhil", "Shambhavi", "Astha"]
roll_no = [4, 1, 3, 2]
marks = [40, 50, 60, 70]

mapped = zip(name, roll_no, marks)
mapped = list(mapped)
print(mapped)

namz, roll_noz, marksz = zip(*mapped)
print(namz)       # ["Manjeet", "Nikhil", "Shambhavi", "Astha"]
print(roll_noz)   # [4, 1, 3, 2]
print(marksz)     # [40, 50, 60, 70]
