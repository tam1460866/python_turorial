import string
import random

LETTERS = string.ascii_letters
# LETTERS = abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
NUMBER = string.digits
# NUMBER = 0123456789
PUNCTUATION = string.punctuation
# PUNCTUATION = !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
# PRINTABLE = string.printable

def get_password_length():
    password_length = input("How long do you want your password:")
    return int(password_length)

def passwords_generator(length=12):
    printable = f'{NUMBER}{LETTERS}{PUNCTUATION}'

    printable = list(printable)
    random.shuffle(printable)

    random_password = random.choices(printable, k=length)
    random_password = ''.join(random_password)
    return random_password

def main():
    password_length = get_password_length()
    password = passwords_generator(password_length)
    print(password)

if __name__ == '__main__':
    main()