'''
    List trong Python = Array in C/C++, Java....

    Kiểu dữ liệu List trong Python là một collection lưu trữ các phần tử theo tứ tự đã cho, 
    có thể thay đổi. Cho phép chứa dữ liệu trùng lặp. List có cấu trúc dữ liệu mà có khả năng 
    lưu giữ các kiểu dữ liệu khác nhau. 
'''

'''
    Creating a List
'''
list1 = ["banana", "apple", "orange", "cherry"]
list2 = [5, 'banana', False, None]
list3 = list()  # list3 = []

string = "Vu Thanh Tai"
print(list(string))
# Ket Qua: ['V', 'u', ' ', 'T', 'h', 'a', 'n', 'h', ' ', 'T', 'a', 'i']

tup = ('A', 'B', 'C')
print(list(tup))
# Ket Qua: ['A', 'B', 'C']

'''
    Iterable List
'''
days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

# use for loop
for day in days:
    print(day)

# use enumerate (tra ve index & element)
for index, day in enumerate(days):
    print(f"The day {index} is {day}") # the day 0 is Mon, ...The day 6 is Sun

for index, day in enumerate(days, start=1):
    print(f"The day {index} is {day}") # the day 1 is Mon, ...The day 7 is Sun

'''
    Access Elements
    Arr[index]
'''
my_list = [1, 2, '3', True]

length = len(my_list)          # length = 4
print(my_list[0])              # 1
print(my_list[length - 1])     # True

'''
    Index Elements
    Arr.index(element)
'''
my_list = [1, 2, 2, '3', '3', '3', True]

length = len(my_list)          # length = 4
print(my_list.index('3'))      # 3
print(my_list.index(1))        # 0
# print(my_list.index(10))     ValueError: 10 is not in list

'''
    Count Elements
    Arr.count(element)
'''
my_list = [1, 2, '3', '3', '3', '3', True]

print(my_list.count('3'))      # 4
print(my_list.count(2))        # 1
print(my_list.count(10))       # 0


'''
    Reverse List
        + list.reverse(): dao nguoc list hien tai
        + reversed(Lst):  copy ra 1 list moi dao nguoc roi tra ve lít moi do
        + Slicing List[::-1]: cắt ra 1 list mới voi thứ tự đảo ngược
'''
lst = [5, 2, 7, 3, 0, 5, 3, 9]
lst.reverse()
print(lst)

lst = [5, 2, 7, 3, 0, 5, 3, 9]
new_list = list(reversed(lst))
print(new_list)
print(lst)

lst = [5, 2, 7, 3, 0, 5, 3, 9]
new_list = lst[::-1]
print(new_list)
print(lst)

'''
    Slicing List
    List[start : End : IndexJump]
       start is empty ==> start = 0
       End is empty   ==> end = length
       IndexJump is empty ==> IndexJump = 1
    Slicing List cat ra 1 list moi va khong dung toi list cu

'''
# Initialize list
#       0   1   3   3   4   5   6
Lst = [50, 70, 30, 20, 90, 10, 50]
#      -7  -6  -5  -4  -3  -2  -1

# Display list
print(Lst[::])      # [50, 70, 30, 20, 90, 10, 50]

# Negative Indexes
print(Lst[-7::1])   # [50, 70, 30, 20, 90, 10, 50]

print(Lst[1:5])     # [70, 30, 20, 90]

#       0  1  2  3  4  5  6
List = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#                   -5 -4 -3 -2 -1

print(List[3:9:2])  # [4, 6, 8]
print(List[::2])    # [1, 3, 5, 7, 9]
print(List[::])     # [1, 2, 3, 4, 5, 6, 7, 8, 9]

# Danh sách đảo ngược có thể được tạo bằng cách sử dụng 
# một số nguyên âm làm đối số IndexJump va Để trống Phần mở đầu và Kết thúc 
print(List[::-1])   # [9, 8, 7, 6, 5, 4, 3, 2, 1]
print(List[::-3])   # [9, 6, 3]
 
# Chúng ta cần chọn giá trị Ban đầu và Kết thúc 
# theo danh sách đảo ngược nếu giá trị IndexJump là âm. 
print(List[:1:-2])  # [9, 7, 5, 3]  IndexJump = -2 nen lay dao  nguoc tu index -1 ket thuc o index 1

# Nếu một số biểu thức cắt được thực hiện không có ý nghĩa 
# hoặc không thể thay đổi thì danh sách trống sẽ được tạo.
print(List[100::2])   # []
print(List[1:1:1])    # []
print(List[:0:])      # []

# Cắt danh sách có thể được sử dụng để sửa đổi
# danh sách hoặc thậm chí xóa các phần tử khỏi danh sách.
original_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]

original_list[2:4] = ['Geeks', 'for', 'Geeks', '!'] # tu vi tri 2 => 3 thay the bang list moi
print(original_list)   # [1, 2, 'Geeks', 'for', 'Geeks', '!', 5, 6, 7, 8, 9]

original_list[:6] = [] # original_list = [1, 2, 'Geeks', 'for', 'Geeks', '!', 5, 6, 7, 8, 9]
print(original_list)   # [5, 6, 7, 8, 9]

# Bằng cách nối các danh sách đã cắt, một danh sách mới có thể được tạo 
# hoặc thậm chí có thể sửa đổi một danh sách đã có từ trước.
original_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]

print(original_list[:3])     # [1, 2, 3]
print(original_list[7:])     # [8, 9]
newList = original_list[:3]+original_list[7:]   # Creating new List
print(newList)               # [1, 2, 3, 8, 9] 

original_list = original_list[::2]+original_list[1::2]   # Changing existing List
print(original_list)         # [1, 3, 5, 7, 9, 2, 4 , 6, 8]

'''
    Operations List
    max(), min(), sum()
'''
my_list = [1, 2, 3]
print(my_list * 2)                # [1, 2, 3, 1, 2, 3]
print(my_list + [100, 'tam12'])   # [1, 2, 3, 100, 'tam12']

list = ['1', '3', '2']
print(max(list))   #Kết quả: 3
list = ['A', 'B', 'C']
print(min(list))   #Kết quả: A
list = [1, 2, 3]
print(sum(list))   #Kết quả: 6


'''
    Add Element or list to List
        append: them 1 phan tu vao list
        extend: them 1 list phan tu vao list
        insert(index, val): them 1 phan tu vao list tai vi tri index

'''
my_list = [1, 2, 3]
my_list.append(100)
print(my_list)     # [1, 2, 3, 100]
my_list.append(('tupe01', 'tupe02'))
print(my_list)     # [1, 2, 3, 100, ('tupe01', tupe02)]

my_list = [1, 2, 3]
my_list.extend([100, 'tam', True])
print(my_list)     # [1, 2, 3, 100, 'tam', True]

list = ['A', 'B', 'C']
list.insert(0, 'Z')
print(list)     # Kết quả: ['Z', 'A', 'B', 'C']
list.insert(2, 'D')
print(list)     # Kết quả: ['Z', 'A', 'D', 'B', 'C']
list.insert(100, 'D')
print(list)     # Kết quả: ['Z', 'A', 'D', 'B', 'C', 'D']
list.insert(100, [1, 2, 3]) 
print(list)     # ['Z', 'A', 'D', 'B', 'C', 'D', [1, 2, 3]]
  
'''
    Replace Values in a List
        Using list indexing
        Using for loop
        Using lambda function
        Using list slicing
'''

# Using list indexing
l = [ 'Hardik','Rohit', 'Rahul', 'Virat', 'Pant']
l[0] = 'Shardul'
print(l)  # ['Shardul', 'Rohit', 'Rahul', 'Virat', 'Pant']

# Using for loop
l = ['Hardik', 'Rohit', 'Rahul', 'Virat', 'Pant']
for i in range(len(l)):
    if l[i] == 'Hardik':
        l[i] = 'Shardul'
  
    if l[i] == 'Pant':
        l[i] = 'Ishan'
print(l)  # ['Shardul', 'Rohit', 'Rahul', 'Virat', 'Ishan']

# Lambda Function l=list(map(lambda x: x.replace(‘old_value’,’new_value’),l))
l = ['Hardik', 'Rohit', 'Rahul', 'Virat', 'Pant']
l = list(map(lambda x: x.replace('Pant', 'Ishan'), l))
print(l) # ['Hardik', 'Rohit', 'Rahul', 'Virat', 'Ishan']

# List Slicing l=l[:index]+[‘new_value’]+l[index+1:]
l = ['Hardik', 'Rohit', 'Rahul', 'Virat', 'Pant']
  
i = l.index('Rahul')
l = l[:i]+['Shikhar']+l[i+1:]
print(l)  # ['Hardik', 'Rohit', 'Shikhar', 'Virat', 'Pant']


'''
    Remove Element or List
        remove(val): Phương thức này có tác dụng xóa phần tử khỏi list.
        pop(index): Phương thức này có tác dụng xóa bỏ phần tử trong list dựa trên index của nó.
                    Mặc định thì index = list[-1] (phần tử cuối cùng trong list).
        clear(): Phương thức này có tác dụng xóa bỏ hết tất cả các phần tử trong list.
        del list[index]: xoa 1 phan tu trong list

'''
list = ['A', 'B', 'C']
list.remove('C')
print(list)        # Kết quả: ['A', 'B']
# list.remove('M') # ValueError: list.remove(x): x not in list

list = ['A', 'B', 'C', 'D', 'E']
list.pop()
print(list)    # Kết quả: ['A', 'B', 'C', 'D']
list.pop(2)
print(list)    # Kết quả: ['A', 'B', 'D']

list = ['A', 'C', 'B', 'E', 'D']
list.clear()
print(list)    # Kết quả: []

list = ['A', 'C', 'B', 'E', 'D']
del list[0]  
print(list)    # Kết quả: ['C', 'B', 'E', 'D']


'''
    Sort List
        mylist.sort(reverse, key): sort truc tiep tren mylist
            + reverse = True thì list sẽ được sắp xếp từ lớn đến bé
            + reverse = False thì list sẽ được sắp xếp theo thứ tự từ bé đến lớn (mac dinh)
            + key là callback def để xử lý list hoặc là một lamda function 
              (thường được dùng để sắp xếp các list tuple hoặc dictionary, 
              xac định sắp xếp theo phần tử nao trong tuple hoặc dictionary).
        sort() không phải là method của tuple và set, nên đối tượng thuộc kiểu này 
        sẽ không gọi được nó

    Sorted List
        sorted(mylist, reverse, key): tao ra 1 list roi moi sort va tra ve list moi do
        sorted dung dc cho ca list, tuple và set 
    
    lưu ý: các phần tử khi sỏt phai cung kieu dữ liệu không dc khac kiểu dữ liệu, 
    chữ hoa và thường sẽ không sort dc, Điều khác cần lưu ý đó là hàm mà được 
    truyền vào key chỉ được phép nhận 1 tham số.

'''
list = ['A', 'C', 'B', 'E', 'D']

list.sort()
print(list)  # Kết quả: ['A', 'B', 'C', 'D', 'E']

list.sort(reverse=True)
print(list)  # Kết quả: ['E', 'D', 'C', 'B', 'A']

def custom_sort(elem):
    return elem[1]    # search theo element thu 2 trong moi phan tu cua tupe

list = [(1, 2), (5, 7), (7, 100), (4, 200)]
list.sort(key=custom_sort)
print(list)   # Kết quả: [(1, 2), (5, 7), (7, 100), (4, 200)]
list.sort(key=lambda x:x[0]) # search theo element thu 1 trong moi phan tu cua tupe
print(list)   # Kết quả: [(1, 2), (4, 200), (5, 7), (7, 100)]


number_list = ['1', '5', '9', '4', '2', '0']
print(sorted(number_list))   # ['0', '1', '2', '4', '5', '9']
print(number_list)           # ['1', '5', '9', '4', '2', '0']

number_list = [1, 2, 3, 4, 5, 6]
print(sorted(number_list, reverse=True))  # [6, 5, 4, 3, 2, 1]
print(sorted(number_list, reverse=False)) # [1, 2, 3, 4, 5, 6]
print(sorted(number_list))                # [1, 2, 3, 4, 5, 6]

list_array = [[1, 3], [4,0], [2,1], [7,3], [9,9]]
print(sorted(list_array, key=lambda x:x[0])) #[[1, 3], [2, 1], [4, 0], [7, 3], [9, 9]]
print(sorted(list_array, reverse=True, key=lambda x:x[0])) #[[9, 9], [7, 3], [4, 0], [2, 1], [1, 3]]

'''
    Copy List
    lst.copy(): tra ra 1 list moi copy tu list cu
'''
fruits = ["apple", "banana", "guava"]
listCopy1 = fruits
listCopy2 = fruits.copy()
fruits.append("kiwi")

print(listCopy1)  # ['apple', 'banana', 'guava', 'kiwi']
print(listCopy2)  # ['apple', 'banana', 'guava']