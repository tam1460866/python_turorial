'''
    Function Arguments
'''
# Arguments bat buoc
def print_name(name, age): # Paramerter name & age bat buoc
    print(f"{name} : {age}")
print_name("tam", 18) # bat buoc phai truyen du 2 Arguments

# Arguments khong bat buoc
def print_name(name, age=18): # Paramerter name bat buoc & age default = 18
    print(f"{name} : {age}")

print_name("tam") # ko bat buoc phai truyen du 2 Arguments
print_name("tam", 20)

# Arguments keyword
def print_name(a, b, c):
    print(f"{a} : {b} : {c}")

print_name(c='tam', a='hao', b='huy')


'''
    Toán tử Splat *

'''

a = [1,2,3]
b = [a,4,5,6]
print(b) # [[1,2,3], 4, 5, 6]

a = [1,2,3]
b = [*a,4,5,6]
print(b) # [1,2,3,4,5,6]


'''
    *args

    Ý nghĩa: Cú pháp *args, ký hiệu * tức là nhận vào một số lượng có thể thay đổi được 
            các args – viết tắt của argument (đối số truyền vào cho hàm).
    Sử dụng ký hiệu *, thì cái biến mà chúng ta gắn nó với ký hiệu * sẽ trở thành một kiểu dữ liệu iterable, 
            nghĩa là lúc này bạn có thể thực hiện những việc như lặp/duyệt qua biến đó, 
            hay sử dụng các hàm bậc cao hơn chẳng hạn như map và filter, v.v… 
'''
# *args for variable number of arguments 
def myFun(*argv):  
    for arg in argv:  
        print (arg) 
    
myFun('Hello', 'Welcome', 'to', 'cafedevn') 

# *args with first extra argument 
def myFun(arg1, *argv): 
    print ("First argument :", arg1) 
    for arg in argv: 
        print("Next argument through *argv :", arg) 
  
myFun('Hello', 'Welcome', 'to', 'cafedev.vn') 


def printScores(student, *scores):
   print(f"Student Name: {student}")
   for score in scores:
      print(score)

printScores("Jonathan",100, 95, 88, 92, 99)

'''
    **kwargs

    **kwargs trong phần định nghĩa hàm (function definitions) được sử dụng dể truyền 
    một số lượng có thể thay đổi được các đối số đã được keyworded – định từ khóa 
    cho một hàm

    kwargs là một kiểu dictionary sẽ ánh xạ (maps) từng keyword (từ khóa) tới mỗi giá trị mà 
    chúng ta truyền vào trong hàm. Đó là lý do vì sao khi chúng ta lặp qua kwargs và 
    in ra các phần tử thì chúng được in ra không theo thứ tự nào cả.
'''
def myFun(**kwargs):  
    for key, value in kwargs.items(): 
        print ("%s == %s" %(key, value)) 
  
myFun(first ='caffedev', mid ='for', last='caffedev.vn')


def myFun(arg1, **kwargs):  
    for key, value in kwargs.items(): 
        print ("%s == %s" %(key, value)) 
  
myFun("Hi", first ='cafedev.vn', mid ='for', last='cafedev.vn') 


def myFun(arg1, arg2, arg3): 
    print("arg1:", arg1) 
    print("arg2:", arg2) 
    print("arg3:", arg3) 
        
args = ("cafedevn", "for", "cafedevn") 
myFun(*args)    # arg1: cafedevn
                # arg2: for
                # arg3: cafedevn
  
kwargs = {"arg1" : "cafedevn", "arg2" : "for", "arg3" : "cafedevn"} 
myFun(**kwargs) # arg1: cafedevn
                # arg2: for
                # arg3: cafedevn

def printPetNames(owner, **pets):
   print(f"Owner Name: {owner}")
   for pet,name in pets.items():
      print(f"{pet}: {name}")

printPetNames("Jonathan", dog="Brock", fish=["Larry", "Curly", "Moe"], turtle="Shelldon")
# Owner Name: Jonathan
# dog: Brock
# fish: ['Larry', 'Curly', 'Moe']
# turtle: Shelldon
'''
    Cách sử dụng *args và **kwargs

    *args là viết tắt của (arguments) đối số, **kwargs là viết tắt của (keyword arguments) đối số từ khóa.
    Sử dụng *args, **kwargs nhưng là quy ước tiêu chuẩn để bắt các đối số vị trí và từ khóa.
    Bạn không thể đặt **kwargs trước *args, nếu không bạn sẽ nhận lỗi,
    Cẩn thận việc trùng lặp tham số từ khóa và **kwargs khi giá trị được định nghĩa sẽ nằm ở **kwarg nhưng nó vô tình dính tên của tham số từ khóa.
    Bạn có thể sử dụng toán tử splat khi gọi function.
'''
def printPetNames(owner, *age, **pets):
   print(f"Owner Name: {owner}")
   for age,(pet,name) in zip(age,pets.items()):
      print(f"{age} : {pet}: {name}")

printPetNames("Jonathan", 2, 4, 3, dog="Brock", fish=["Larry", "Curly", "Moe"], turtle="Shelldon")
# Owner Name: Jonathan
# 2 : dog: Brock
# 4 : fish: ['Larry', 'Curly', 'Moe']
# 3 : turtle: Shelldon