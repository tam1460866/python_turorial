'''
    Lambda Funcion
'''
def cube(y):
    return y*y*y
 
lambda_cube = lambda y: y*y*y
 
# defined function
print(cube(5))
 
# using the lambda function
print(lambda_cube(5))

# =====================================================
print((lambda x, y : x - y)(2,5))

# =====================================================
sum = lambda a, b, c : a + b + c
print(sum(2, 3, 4)) # 9

# =====================================================
coordinate = [(1, 2), (3, 8), (5, 3)]
sort_arr_1 = sorted(coordinate, key=lambda p: p[0]) # [(1, 2), (3, 8), (5, 3)]
sort_arr_2 = sorted(coordinate, key=lambda p: p[1]) # [(1, 2), (5, 3), (3, 8)]
print(sort_arr_1)
print(sort_arr_2)

# =====================================================
x ="GeeksforGeeks"
(lambda x : print(x))(x) # GeeksforGeeks

# =====================================================
tables = [lambda x=x: x*10 for x in range(1, 11)] # [lambda x=1: 1*10, lambda x=2: 2*10....lambda x=10: 10*10]
for table in tables:
    print(table())

# =====================================================
Max = lambda a, b : a if(a > b) else b 
print(Max(1, 2)) # 2

# =====================================================
# tim phan tu lon thu 2 trong moi list
Lst = [[3,2,4],[1, 4, 64, 16],[3, 6, 9, 12]]

# sortList
sortList = lambda x: (sorted(i) for i in x)
print(list(sortList(Lst)))
secondLargest = lambda x, f : [y[len(y)-2]for y in f(x)]
print(secondLargest(Lst, sortList)) # [3, 16, 9]

# =====================================================
li = [5, 7, 22, 97, 54, 62, 77, 23, 73, 61]
final_list = list(filter(lambda x: (x%2 != 0) , li))
print(final_list)   # [5, 7, 97, 77, 23, 73, 61]

ages = [13, 90, 17, 59, 21, 60, 5]
adults = list(filter(lambda age: age>18, ages))
print(adults)    # [90, 59, 21, 60]

# =====================================================
li = [5, 7, 22, 97, 54, 62, 77, 23, 73, 61]
final_list = list(map(lambda x: x*2, li))
print(final_list)   # [10, 14, 44, 194, 108, 124, 154, 46, 146, 122]

animals = ['dog', 'cat', 'parrot', 'rabbit']
uppered_animals = list(map(lambda animal: str.upper(animal), animals))
print(uppered_animals)   # ['DOG', 'CAT', 'PARROT', 'RABBIT']

# =====================================================
from functools import reduce
li = [5, 8, 10, 20, 50, 100]
sum = reduce((lambda x, y: x + y), li) # (((((5+8)+10)+20)+50)+100) = 193ßß
print (sum)

import functools
 
# initializing list
lis = [ 1 , 3, 5, 6, 2, ]
print ("The maximum element of the list is : ",end="")
print (functools.reduce(lambda a,b : a if a > b else b,lis)) # The maximum element of the list is : 6