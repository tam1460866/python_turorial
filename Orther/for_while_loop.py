'''
    For
'''

my_list = [1, 5, 8, 9]
for item in my_list:
    print(item)

my_list = [1, 5, 8, 9]
for index, item in enumerate(my_list):
    print(index, item)

my_dict = {'a': 'tam', 'b': 'hao', 'c': 'anh'}
for key, val in my_dict.items():
    print(f'{key} : {val}')

for key in my_dict.keys():
    print(f'{key} : {my_dict[key]}')

for val in my_dict.values():
    print(f'{val}')

'''
    While
'''

while True:
    msg = input("Please enter a message: ")
    if msg == 'exit':
        break
    print(msg)